local M = {}


M.general = {
  n = {
    ["<S-l>"] = {
      ":bnext<CR>",
      "Move next buffer"
    },
    ["<S-h>"] = {
      ":bprevious<CR>",
      "Move previous buffer"
    },
    ["<A-j>"] = {"<Esc>:m .+1<CR>==", "Move text down"},
    ["<A-k>"] = {"<Esc>:m .-2<CR>==", "Move text up"},
  },
  v = {
    ["<A-j>"] = {":m '>+1<CR>gv=gv", "Move text down"},
    ["<A-k>"] = {":m '<-2<CR>gv=gv", "Move text up"},
  }
}

M.nvimtree = {
  n = {
    -- toggle
    ["<leader>e"] = { "<cmd> NvimTreeToggle <CR>", "Toggle nvimtree" },
  },
}

M.nvterm = {
  t = {
    ["<C-\\>"] = {
      function()
        require("nvterm.terminal").toggle "float"
      end,
      "Toggle floating term",
    },
  },
  n = {
    ["<C-\\>"] = {
      function()
        require("nvterm.terminal").toggle "float"
      end,
      "Toggle floating term",
    },
  }
}

M.dap = {
  plugin = true,
  n = {
    ["<leader>db"] = {
      "<cmd> DapToggleBreakpoint <CR>",
      "Add breakpoint at line",
    },
    ["<leader>dr"] = {
      "<cmd> DapContinue <CR>",
      "Start or continue the debugger",
    }
  }
}

return M
