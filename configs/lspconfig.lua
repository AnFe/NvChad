local base = require("plugins.configs.lspconfig")
local on_attach = base.on_attach
local capabilities = base.capabilities

local lspconfig = require("lspconfig")
local servers = { "clangd", "csharp_ls"}
-- local servers = { "clangd", "rust_analyzer", "csharp_ls"}
-- rust_analyzer removed due to the rust debugger addon
for _, server in ipairs(servers) do
  lspconfig[server].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end
