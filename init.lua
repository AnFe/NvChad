local o = vim.o
local g = vim.g

o.scrolloff = 8                           -- minimal number of screen lines to keep above and below the cursor
o.sidescrolloff = 8                       -- minimal number of screen columns either side of cursor if wrap is `false`
o.whichwrap = "bs<>[]hl"                  -- which "horizontal" keys are allowed to travel to prev/next line
o.timeoutlen = 400


g.encoding = "UTF-8"
