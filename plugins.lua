local plugins = {
  {
    "rcarriga/nvim-dap-ui",
    event = "VeryLazy",
    dependencies = "mfussenegger/nvim-dap",
    config = function()
      local dap = require("dap")
      local dapui = require("dapui")
      dapui.setup()
      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
      end
    end
  },
   {
    "jay-babu/mason-nvim-dap.nvim",
    event = "VeryLazy",
    dependencies = {
      "williamboman/mason.nvim",
      "mfussenegger/nvim-dap",
    },
    opts = {
      handlers = {},
    }
  },
  {
    "hrsh7th/nvim-cmp",
    opts = function()
      return require("custom.configs.cmp")
    end,
  },
  {
    "mfussenegger/nvim-dap",
    config = function (_, _)
      require("core.utils").load_mappings("dap")
    end
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    event = "VeryLazy",
    opts = function()
      return require "custom.configs.null-ls"
    end,
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end
  },
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "clangd",
        "clang-format",
        "codelldb",
        "rust-analyzer"
      },
    },
  },
  {
    "rust-lang/rust.vim",
    ft = "rust",
    init = function()
      vim.g.rustfmt_autosave = 1
    end
  },
  {
    "machakann/vim-sandwich",
    lazy = false,
  },
  {
  'mrcjkb/rustaceanvim',
  version = '^3', -- Recommended
  ft = { 'rust' },
    config = function()
      -- vim.g.rustaceanvim = {
        require "custom.configs.rustaceanvim"
      -- }
    end
  },
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function()
      return require "custom.configs.treesitter"
    end,
  },
  {
    "nvim-tree/nvim-tree.lua",
    opts = {
       renderer = {
        icons = {
          glyphs = {
            default = "",
            folder = {
              empty = "",
              empty_open = "",
            },
            git = {
              unstaged = "",
              staged = "S",
              untracked = "U",
            },
          },
        },
      },
    },
  },
  {
    "NvChad/nvterm",
    opts = function()
      return require "custom.configs.nvterm"
    end,
  },
  {
    "xiyaowong/transparent.nvim",
    lazy = false,
    config = function()
      vim.cmd([[hi StatusLine ctermbg=0 cterm=NONE]])
      require("transparent").setup({
        groups = { -- table: default groups
          'Normal', 'NormalNC', 'Comment', 'Constant', 'Special', 'Identifier',
          'Statement', 'PreProc', 'Type', 'Underlined', 'Todo', 'String', 'Function',
          'Conditional', 'Repeat', 'Operator', 'Structure', 'LineNr', 'NonText',
          'SignColumn', 'CursorLineNr', 'EndOfBuffer',
        },
        extra_groups = {
          "NvimTreeNormal",
          "NormalFloat",
          "FloatBorder",
          "TablineFill",
          "NvimTreeWinSeparator",
          "NvimTreeNormalNC"
        }
      })
    end
  }
}
return plugins
