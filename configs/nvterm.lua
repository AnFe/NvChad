local options = {
  terminals = {
    type_opts = {
      float = {
        row = 0.2,
        col = 0.15,
        width = 0.75,
        height = 0.85,
      }
    }
  }
}


return options
