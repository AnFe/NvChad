vim.api.nvim_create_autocmd({ "LspAttach" }, {
  desc = "Disable semantic tokens for rust",
  group = vim.api.nvim_create_augroup("disable-rustacean-colors", { clear = true }),
  callback = function(opts)
    if vim.bo[opts.buf].filetype == "rust" then
      local client = vim.lsp.get_client_by_id(opts.data.client_id)
      client.server_capabilities.semanticTokensProvider = nil
    end
  end,
})

vim.g.rustaceanvim = {
  tools = {
    hoved_actions = {
      replace_builtin_hover = false,
    },
  },
  server = {
    on_attach = function(_, bufnr)
      print(bufnr)
      local lsp = vim.lsp.buf
      local t = function(func)
        return string.format("<cmd> Telescope %s<cr>", func)
      end
      local prefix = "LSP: "
      local function bmap(mode, key, remap, desc)
        vim.keymap.set(mode, key, remap, { buffer = bufnr or true, desc = desc })
      end
      bmap("n", "<leader>ca", lsp.code_action, prefix .. "Code Action")
      bmap("n", "<leader>cd", t("diagnostics"), prefix .. "Diagnostics")
      bmap("n", "<leader>cr", lsp.rename, prefix .. "Rename")
      bmap("n", "<leader>cs", t("lsp_document_symbols"), prefix .. "Document Symbols")
      bmap("n", "gd", t("lsp_definitions"), prefix .. "Goto Definition")
      bmap("n", "gr", t("lsp_references"), prefix .. "Goto References")
      bmap("n", "gD", lsp.declaration, prefix .. "Goto Declaration")
      bmap("n", "gI", t("lsp_implementations"), prefix .. "Goto Implementations")
      bmap("n", "gy", t("lsp_type_definitions"), prefix .. "Goto Type Definition")
      bmap("n", "K", lsp.hover, prefix .. "Hover Documentation")
      -- bmap("n", "K", "<cmd>RustLsp hover actions<CR>", prefix .. "Hover Documentation")
      bmap("n", "gK", lsp.signature_help, prefix .. "Signature Help")
      bmap("i", "<C-k>", lsp.signature_help, prefix .. "Signature Help")
    end,
    settings = {
      ["rust-analyzer"] = {
        cargo = {
          allFeatures = true,
          loadOutDirsFromCheck = true,
          runBuildScripts = true,
        },
        checkOnSave = true,
        inlayHints = {
          lifetimeEllisionHints = {
            enable = true,
            useParameterNames = true
          }
        }
      },
    },
  },
  dap = {},
}

